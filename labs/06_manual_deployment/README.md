# Manual Deployment to Kubernetes

*By the end of this lab, you will:*

- Launch an nginx webserver on your Kubernetes cluster
- Understand the pain of a manual Kubernetes deploy

---

Save this file as `~/sfs/mywebserver/webserver-manual.yaml`

```shell
touch ~/sfs/mywebserver/webserver-manual.yaml
edit ~/sfs/mywebserver/webserver-manual.yaml
```

```yaml
# The Deployment object expresses the desired state for your app
kind: Deployment
apiVersion: apps/v1
metadata:
  name: mywebserver-dev
  labels:
    app: webserver
    course: introtodevops
    environment: dev
spec:
  # Specify how many pods of this type k8s will run in this deployment
  replicas: 1
  selector:
    matchLabels:
      app: webserver
      course: introtodevops
      environment: dev
  template:
    metadata:
      labels:
        app: webserver
        course: introtodevops
        environment: dev
    spec:
      # Specify the container(s) to run inside each pod.
      containers:
        - name: webserver
          image: nginx
          imagePullPolicy: IfNotPresent
          ports:
            - name: default
              containerPort: 80
              protocol: TCP
---
# Configure the app to be used by other apps
kind: Service
apiVersion: v1
metadata:
  name: mywebserver-dev
  labels:
    app: webserver
    course: introtodevops
    environment: dev
spec:
  type: NodePort
  ports:
    - port: 80
      targetPort: default
      protocol: TCP
      name: svcport
  # Selects pods to send traffic to by matching selectors to pod labels
  selector:
    app: webserver
    course: introtodevops
    environment: dev
---
# Create an ingress object to connect the pod(s) to the Ingress Controller we created in the previous lab
kind: Ingress
apiVersion: extensions/v1beta1
metadata:
  name: mywebserver-dev
  labels:
    app: webserver
    course: introtodevops
    environment: dev
  annotations:
    kubernetes.io/ingress.class: nginx
spec:
  rules:
    - host: mywebserver
      http:
        paths:
          - path: /
            backend:
              serviceName: mywebserver-dev
              servicePort: svcport

```
---

logical objects this will spin up in our new infrastructure (when we do 'kubectl apply...' command below'):

![image](gke-cluster-logical-view.png?)

---

expanded detail of k8s cluster Service (Selectors and Labels):

![image](k8s-labels-selectors.png?)
---

Labels and Selectors are critical to the basic understanding of the deploy/replicaset/pod/service interfaces

#### Manual Deploy

1. Double-check that you're still on the right cluster

  ```bash
  kubectl get nodes
  ```

2. Apply your manifest to your cluster

  ```bash
  kubectl apply -f ~/sfs/mywebserver/webserver-manual.yaml
  ```

  This should output
  ```
  deployment.apps "webserver-dev" created
  service "webserver-dev" created
  ingress.extensions "webserver-dev" created
  ```

3. See what it created:

  ```bash
  kubectl get deployment,service,ingress
  ```

  Or the shorter version:

  ```bash
  kubectl get deploy,svc,ing
  ```

  Regardless, you should see something like this:

  ```
  NAME                                    DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
  deployment.extensions/mywebserver-dev   1         1         1            1            2m

  NAME                      TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
  service/kubernetes        ClusterIP   10.70.0.1      <none>        443/TCP         2m
  service/mywebserver-dev   NodePort    10.70.12.123   <none>        80:31219/TCP    2m

  NAME              HOSTS         ADDRESS         PORTS     AGE
  mywebserver-dev   mywebserver   35.222.16.177   80         2m
  ```

4. Test it!

  ```bash
  curl -H "Host: mywebserver" $(kubectl -n ingress-nginx get svc ingress-nginx-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}')

  #
  ```

  This sends a custom host header to an IP we programmatically derive from our cluster.  Since we don't want to to that every time, we need to leverage the DNS configuration we implemented at the end of the last lab.

---

### Using a Hostname

1. `edit ~/sfs/mywebserver/webserver-manual.yaml` and find this line:

  ```yaml
      - host: mywebserver
  ```

  and replace it with your DNS name.  (**Make sure you don't change the indentation.**)

  For example:

  ```yaml
      - host: dev.$YOURNAME.introtodevops.com
  ```

  Save your `webserver-manual.yaml` and re-apply your changes:

  ```bash
  kubectl apply -f ~/sfs/mywebserver/webserver-manual.yaml
  ```

  Make sure your change is implemented:

  ```bash
  kubectl get ing mywebserver-dev
  ```

  Should now show your updated host.  We're going to be using the following command to extract the host directly in the next step:

  ```bash
  kubectl get ing mywebserver-dev -o jsonpath='{.spec.rules[0].host}'
  ```

2. Test it the easy way:

  If you're a GUI-centric user, you should be able to open your webpage in a browser:

  ```bash
  ### Windows and MacOS ###
  open http://$(kubectl get ing mywebserver-dev -o jsonpath='{.spec.rules[0].host}')
  ## Linux ##
  xdg-open http://$(kubectl get ing mywebserver-dev -o jsonpath='{.spec.rules[0].host}')
  ```

  Note this is just a fancy way of opening dev.$YOURNAME.introtodevops.com in a browser!

  You should see the **nginx** welcome page.  ("**Welcome to nginx! ...**")

  If you're awesome like your teacher, you'll eschew the browser for anything that can be done in the terminal, like this:

  ```bash
  curl $(kubectl get ing mywebserver-dev -o jsonpath='{.spec.rules[0].host}')
  ## or, for example... ##
  curl dev.teacher.IntroToDevOps.com
  ```

  You should see something like this:

  ```html
  <!DOCTYPE html>
  <html>
  <head>
  <title>Welcome to nginx!</title>
  <style>
      body {
          width: 35em;
          margin: 0 auto;
          font-family: Tahoma, Verdana, Arial, sans-serif;
      }
  </style>
  </head>
  <body>
  <h1>Welcome to nginx!</h1>
  <p>If you see this page, the nginx web server is successfully installed and
  working. Further configuration is required.</p>

  <p>For online documentation and support please refer to
  <a href="http://nginx.org/">nginx.org</a>.<br/>
  Commercial support is available at
  <a href="http://nginx.com/">nginx.com</a>.</p>

  <p><em>Thank you for using nginx.</em></p>
  </body>
  </html>
  ```

---

| Previous: [GCP & GKE](/labs/05_gcp_and_gke) | Next: [K8s to GitLab Auth](/labs/07_auth_kubernetes_to_gitlab) |
|---:|:---|
