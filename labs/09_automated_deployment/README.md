# (Better) Automated Deployment

*By the end of this lab, you will:*

- Have more control over your deployments
- See how to create more extensible deployment templates with Helm

---

If we use the `latest` tag (`mywebserver:latest`) for our deploys this could cause problems:

- When troubleshooting, we don't really know which version of our app we're running.
- If a Kubernetes node crashes, its replacement could pull a newer version of the app.
- Without forcing a `docker pull`, Kubernetes might not update as expected.

---

### Required Working Directory

```shell
cd ~/sfs/mywebserver
```

#### Pre-Start Cleanup

- Clean up your manual deploy, service, and ingresses with

  ```shell
  # Remove our webserver stuff
  kubectl delete deploy,svc,ing mywebserver-dev

  # Verify that it's gone (THIS SHOULD ERROR)
  curl $(kubectl get ing mywebserver-dev -o jsonpath='{.spec.rules[0].host}')
  ```

### Helm Chart & Template

1. Install Helm if needed (shouldn't be required for CloudShell)

  ```shell
  which helm ||
    curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
  ```

2. Create the directory structure for a Helm chart

  ```shell
  mkdir -p ~/sfs/mywebserver/deploy/templates
  ```

3. Save the following chart metadata at `~/sfs/mywebserver/deploy/Chart.yaml`

  ```shell
  touch ~/sfs/mywebserver/deploy/Chart.yaml
  edit ~/sfs/mywebserver/deploy/Chart.yaml
  ```

  ```yaml
  apiVersion: v1
  appVersion: "1.0"
  description: Web Server Deploy Template
  name: mywebserver
  version: 1.0.0
  ```

4. Save the following at `~/sfs/mywebserver/deploy/templates/webserver.yaml`.  (We'll examine the important bits in the next step.)

  ```shell
  touch ~/sfs/mywebserver/deploy/templates/webserver.yaml
  edit ~/sfs/mywebserver/deploy/templates/webserver.yaml
  ```

  ```yaml
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: mywebserver-{{ .Values.environment }}
    labels:
      app: webserver
      course: introtodevops
      environment: {{ .Values.environment }}
  spec:
    replicas: 1
    selector:
      matchLabels:
        app: webserver
        course: introtodevops
        environment: {{ .Values.environment }}
    template:
      metadata:
        labels:
          app: webserver
          course: introtodevops
          environment: {{ .Values.environment }}
      spec:
        imagePullSecrets:
          - name: regcred
        containers:
          - name: webserver
            image: {{ .Values.image }}
            imagePullPolicy: IfNotPresent
            ports:
              - name: default
                containerPort: 80
                protocol: TCP
  ---
  apiVersion: v1
  kind: Service
  metadata:
    name: mywebserver-{{ .Values.environment }}
    labels:
      app: webserver
      course: introtodevops
      environment: {{ .Values.environment }}
  spec:
    type: NodePort
    ports:
      - port: 80
        targetPort: default
        protocol: TCP
        name: svcport
    selector:
      app: webserver
      course: introtodevops
      environment: {{ .Values.environment }}
  ---
  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    name: mywebserver-{{ .Values.environment }}
    labels:
      app: webserver
      course: introtodevops
      environment: {{ .Values.environment }}
    annotations:
      kubernetes.io/ingress.class: nginx
  spec:
    rules:
      - host: {{ .Values.host }}
        http:
          paths:
            - path: /
              backend:
                serviceName: mywebserver-{{ .Values.environment }}
                servicePort: svcport
  ```

5. Examine the differences between the files

  ```shell
  diff --side-by-side --width=140 webserver-manual.yaml deploy/templates/webserver.yaml
  ```

6. Examine the Helm values we've added

  ```shell
  grep -oe "{{.*}}" deploy/templates/webserver.yaml | sort | uniq
  ```

  You should see

  ```
  {{ .Values.environment }}
  {{ .Values.host }}
  {{ .Values.image }}
  ```

  These are the variables we need to feed into the Helm template to generate a working manifest.  The `.Values` object is pretty much just a container for variables.  So `environment`, `host`, and `image` are the required variables.

7. Manually render and examine the template

  ```shell
  helm template \
    --set image=MyTestImage \
    --set host=MyTestHost \
    --set environment=MyTestEnvironment \
    deploy
  ```

### Using Helm With GitLab CI

1. Update the CI deploy job to use the template.

  **Like before, you can use this as a starting point.  But make sure to provide your own (correct) values for the `variables` in the `deploy_webserver` job!**

  ```yaml
  build_and_test_webserver:
    stage: build
    image: docker:stable
    services:
      - docker:dind
    script:
      - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} -t ${CI_REGISTRY_IMAGE}:latest .
      - docker run -d -p8080:80 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
      - docker logs mywebserver
      - apk add curl
      - curl docker:8080
      - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
      - docker push ${CI_REGISTRY_IMAGE}
      - echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}

  deploy_webserver:
    stage: deploy
    image: google/cloud-sdk:latest
    variables:
      CLUSTER: standard-cluster-1
      REGION: us-central1-a
      PROJECT: i2do-with-k8s
      INGRESS_HOST: dev.teacher.introtodevops.com
      K8S_ENVIRONMENT: dev
    before_script:
      # Connect to the Kubernetes cluster
      - echo ${GCP_AUTH_KEYFILE} > /root/.config/keyfile.json
      - gcloud auth activate-service-account --key-file=/root/.config/keyfile.json
      - gcloud container clusters get-credentials ${CLUSTER} --region ${REGION} --project ${PROJECT}
    script:
      # Install Helm for template parsing
      - curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
      - >
        helm template \
          --set image=${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} \
          --set host=${INGRESS_HOST} \
          --set environment=${K8S_ENVIRONMENT} \
          deploy > manifest.yaml
      - kubectl apply -f manifest.yaml
  ```

2. Commit your changes:

  ```shell
  git add -A
  git commit -m 'add Helm template'
  git push
  ```

3. Navigate to your project in GitLab.com and observe the pipeline.

4. When the pipeline has completed successfully...

  ```shell
  kubectl get deploy,svc,ing
  ```

  What do you see?

  What is new/different?

5. Examine the output of this command.

  ```shell
  kubectl get deploy,svc,ing mywebserver-dev -o yaml
  ```

  - Verify the object names
  - Verify the environment labels
  - Verify the host name of the ingress
  - Verify the container image

6. Check out your web page:

  ```shell
  curl $(kubectl get ing mywebserver-dev -o jsonpath='{.spec.rules[0].host}')
  ```

---

| Previous: [GitLab to K8s Auth](/labs/08_auth_gitlab_to_kubernetes) | Next: [Everything!](/labs/10_everything) |
|---:|:---|
