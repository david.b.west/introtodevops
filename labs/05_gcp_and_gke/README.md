# Build a GKE cluster

Kubernetes the easy way: using GCP

*By the end of this lab, you will:*

- Create a GCP project from the GUI
- Connect to GCP project from the CLI
- Build a Kubernetes cluster from the GUI
- Build a Kubernetes cluster from the CLI
- Run some basic `kubectl` commands

---

Recommended working directory: `~/sfs/work-dir/cloud-infrastructure`

---

![image](gcp-global-regions.png)

**gcp infrastructure:**
- projects, cluster, regions, zones
- 11 regions, 33 zones, 100+ pops
- k8s node pools, control plane

### Verify GCP Access & Create a Project

Make sure you can sign in to the [GCP Console](https://console.cloud.google.com/)

1. Click the project selector menu
  ![project selector menu](project_selector_dropdown.png)
2. Click the `New Project` button in the top right corner of the modal that appears
3. Give your project a unique name
4. Make sure the location and organization are correct (instructor can verify)
5. Click the `Create` button

---

### Trigger API Enablement

Google Cloud Platform has an interesting quirk: everything is off by default.  The easiest way to enable API's/features is just to navigate to the appropriate page in the console.  This can take a few minutes, so we're going to do it now.

1. Go to the [GCP Console](https://console.cloud.google.com/)
2. Use the project selector menu (see picture above) to select the correct project for this class
  - If you don't see your project, click the `All` tab, or...
  - Use the search functionality
  - If you still can't find it, call the instructor over  :p
3. Pin `Kubernetes Engine` for easy access
  1. Click the hamburger menu at the top left corner of the console
  2. Find `Kubernetes Engine`
  3. Click the pin
4. Click `Kubernetes Engine` in the menu

If the notifications don't pop up automatically, you can show them by clicking the bell icon at the top right of the console.  You should see the following notifications:
- `Create Project ...`
- `Initializing Kubernetes Engine for project ...`
- `Initializing Compute Engine for project ...`

There may be multiples - that's okay.  You can move on to the next section before these finish.

---

### Configure the CLI

In your CloudShell environment, run `gcloud init`

You should see some informational text followed by:

```
Pick configuration to use:
 [1] Re-initialize this configuration [cloudshell-24213] with new settings
 [2] Create a new configuration
```

Select option 1, then proceed through the subsequent questions

- Make sure you select the correct account/email
- Make sure you select the correct project
  - This will likely include a numeric identifier in addition to the name you provided
  - E.g. `i2do-teacher-283521` instead of just `i2do-teacher`
- Configure a default Compute Region and Zone
  - Select one of the `us-central1` options

Run `gcloud config configurations list` and verify that your configuration has the values you selected for account, project, and the default Zone/Region.

```
aayore@cloudshell:~/sfs/mywebserver (aarontest-283521)$ gcloud config configurations list
NAME              IS_ACTIVE  ACCOUNT           PROJECT           COMPUTE_DEFAULT_ZONE  COMPUTE_DEFAULT_REGION
cloudshell-24213  True       aayore@sofree.us  aarontest-283521  us-central1-f         us-central1
```

---

### Launch a Kubernetes Cluster from the GUI

1. Log in to the [console](https://console.cloud.google.com)
2. Select your project from the drop-down at the top
3. Navigate to `Kubernetes Engine` console
4. Click the `Create cluster` button

![image](GKE-Cluster-Setup0.png)

5. Under `Cluster basics` in the `Name` field, name it `<your-name>-cluster`

  (more written instructions continue below this graphic)

![image](GKE-Cluster-Setup.png)

6. Select `default-pool` drop-down, Click `Nodes`, click the "Series" drop-down and select 'N1'. Under that, click the 'Machine type' drop-down and select 'g1-small' listed in the table below; _do not click the `Create` button!_
7. On the same screen, make sure `Enable preemptible nodes` is checked
8. Find the `Equivalent REST or command line` link at the bottom of the screen.
9. Click the `command line` link.
10. Save this gnarly command in a new file at `~/sfs/work-dir/new-cluster.sh`
  ```
  touch ~/sfs/work-dir/new-cluster.sh
  edit ~/sfs/work-dir/new-cluster.sh
  ```


---

### Launch a Kubernetes Cluster From the CLI

- Add `#!/bin/bash -eu` as a new line to the top of the file
  - `edit ~/sfs/work-dir/new-cluster.sh`

```shell
# Mark the file executable
chmod +x ~/sfs/work-dir/new-cluster.sh

# Build a cluster!
~/sfs/work-dir/new-cluster.sh
```

GKE cluster infrastructure we just created:

![image](gke-cluster-infrastructure-view.png)

That was easy, right?  You now have a script that you can update or modify to create new clusters as necessary!

---

## Delete a Cluster

- You can delete this cluster (trash can icon) and recreate it easily by typing `~/sfs/work-dir/new-cluster.sh` again!

---

<!-- Note: Do we want to put CLUSTER_NAME and CLUSTER_ZONE in our .env file? -->

## `kubectl` Auth

```bash
# Configure
# (You can easily copy this command from the GCP/GKE UI with the `Connect` button)
gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${CLUSTER_ZONE}

# Verify
kubectl get nodes # >>> Make sure `${CLUSTER_NAME}` is in the output
```

<!--

**Deprecated:**
>
> On GKE clusters v1.11.x and earlier, you have to enable RBAC by creating a binding to give your account `cluster-admin` permissions.
>
> ```bash
> # Grant your account admin access:
> kubectl create clusterrolebinding cluster-admin-binding-$(date "+%Y-%m-%d") \
>   --clusterrole cluster-admin \
>   --user $(gcloud config get-value account)
> ```

NOTE: NGINX DOCS STILL SAY THIS IS REQUIRED!!!  THIS NEEDS TESTING!!!

More information regarding the [RBAC implementation w/ GKE](https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control).

-->

---

## Kubernetes Nginx Ingress

We can configure the Nginx Ingress on our cluster with a single command:

```bash
# https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud/deploy.yaml

# This line is just here to move the scroll bar.  Do not remove.
```

This command creates a number of new objects on your Kubernetes cluster.  A brief summary will be shown in the terminal.

Next, we want to put your cluster in DNS.  Run this command and watch the output:

```bash
watch kubectl -n ingress-nginx get service
```

When `EXTERNAL-IP` changes from `<pending>` to an IP, type Ctrl+C to exit the `watch` command.  Then work with your teacher to complete the configuration in DNS.

Ultimately, we're going to create a wildcard record for your cluster's external IP address.  This means that something like `*.$YOURNAME.IntroToDevOps.com` will route all traffic to the `$YOURNAME`'s Kubernetes cluster.

<!--
Deprecated as of v1.11.x:
```
# Remove the admin access for your account:
kubectl delete clusterrolebinding cluster-admin-binding-$(date "+%Y-%m-%d")
```
-->

---

| Previous: [Continuous Integration](/labs/04_continuous_integration) | Next: [Manual Deployment](/labs/06_manual_deployment) |
|---:|:---|
