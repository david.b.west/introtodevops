# Intro to DevOps

---

### Class Goal

DevOps is an exceptionally broad discipline.  It requires development expertise, operations sensibility, an immense capacity for troubleshooting, eagerness to learn, and - maybe most importantly - a broad base of knowledge around tools that can be stitched together to accomplish the goal.  

This class will provide you with a very basic framework for workflow automation.  Every commit to a repo will be built, tested, and deployed.

There is no Holy Grail - every environment is different.  This course attempts to use mostly free, open-source software to show that workflow automation is a real thing that can be done.  We'll cover one way to do it, but there are many, many other ways to accomplish the same thing.  (I've also done a GitHub --> CircleCI --> AWS/Mesos/Marathon setup that does something very similar to what we'll see in this course.)

---

### Success Metric

Measure - or estimate - the time it takes for a code commit to be deployed in your production environment.  This is the main metric we'll use to measure the success of the DevOps workflow we build in this class.

---

### Prerequisites:

- Before starting this class, you should have a basic familiarity with a Unix-like shell (sh, bash, zsh, ksh, etc.)
- Laptop
  - A web browser - Chrome (or a Chrome-based browser like Brave) is probably best for this class
- [Create a Google Cloud account](https://cloud.google.com/)
  - This should ideally be a personal account (unless you have specific permission to spend money in your company's account)
  - New users should be eligible for [$300 starter credit](https://cloud.google.com/free/)
- [Create a GitLab account](https://gitlab.com/users/sign_in)
  - If you already have one, you can disregard this
  - If you create a new one, you might consider making it something that you could take from company to company

***

### Following Instructions

Please follow the documented and stated instructions as closely as possible.  This will help mitigate issues that arise due to funky configurations.  As mentioned above, we'll be stitching together a number of tools.  This means that the labs are very inter-dependent, and an innocent deviation in an early lab could complicate a later lab.

I encourage all students to experiment and explore the material.  Making it your own and having fun with it will probably increase the functional utility of this class immensely.  I'm happy to help with any extracurricular questions and/or interests related to the material, but please try the extra stuff after completing the suggested stuff.  And maybe in a different subdirectory.  :)

***

# Labs

### 00. [Bash and CloudShell](/labs/00_bash_cloudshell)

### 01. [GitLab](/labs/01_gitlab)

### 02. [Docker](/labs/02_docker)

### 03. [Webserver](/labs/03_webserver)

### 04. [Continuous Integration](/labs/04_continuous_integration)

### 05. [GCP & GKE](/labs/05_gcp_and_gke)

### 06. [Manual Deployment](/labs/06_manual_deployment)

### 07. [Authenticating Kubernetes to GitLab](/labs/07_auth_kubernetes_to_gitlab)

### 08. [Authenticating GitLab to Kubernetes](/labs/08_auth_gitlab_to_kubernetes)

### 09. [Automated Deployment](/labs/09_automated_deployment)

### 10. [Everything](/labs/10_everything) (a.k.a. The Easy Lab)

***

### Bonus Labs

### + [Job Templates & Environments](/labs/11_one_more_step)

### + [Live Testing & Variable Abstraction](/labs/12_yet_another_step)

---

### Recommended Reading
- [The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262509/), by Gene Kim
  - This is a rewrite of *The Goal* for the modern age.
- [The Goal](https://www.amazon.com/Goal-Process-Ongoing-Improvement/dp/0884271951/), by Eliyahu Goldratt
  - This is the original, and I think you'll benefit from doing the modern adaptation yourself.
