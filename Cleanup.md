## Cleanup

This is a list of things we created in this course.

These might be costing you money if you don't clean them up:
- Elastic IP at Amazon
- CloudFormation stack at Amazon

These probably aren't costing you money, but you might want to clean them up anyway:
- SSH key pair at Amazon
- MyWebserver repository at GitLab
- MyWebserver local repository
- work-dir local repository
- Local Docker stuff:
  - `docker ps` to see running containers, `docker rm` to remove them
  - `docker images` to see Docker images on your system, `docker rmi` to remove them
